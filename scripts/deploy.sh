#!/bin/sh -el

helm init --client-only
mkdir -p ./public
cd public
echo "User-Agent: * " > ./robots.txt
echo "Disallow: /" >> ./robots.txt
helm package ../charts/* --destination ./
helm repo index --url https://${CI_PROJECT_NAMESPACE}.gitlab.io/${CI_PROJECT_NAME} .
